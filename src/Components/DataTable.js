import { Container, Grid, Pagination, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from "@mui/material";
import { useEffect, useState } from "react";
const DataTable =() =>{
    const numRow = 3; // Số dòng trên một trang
    const [rows ,setRows] = useState([]);
    const [totalPage , setTotalPage] = useState(0);
    const [curPage , setCurPage] = useState(1);

    const fetchApi = async(url) =>{
        const response = await fetch(url);
        
        const data = response.json();

        return data
    }

    
    useEffect(() =>{
        fetchApi("https://jsonplaceholder.typicode.com/users")
            .then((response) =>{
                setTotalPage(Math.ceil(response.length/numRow));
                setRows(response.slice((curPage-1) * numRow,curPage * numRow));
            })
            .catch((error) =>{
                console.log(error.message);
            })
    },[curPage])

    const handleChangePage = (event,value) =>{
        setCurPage(value);
    }

    

    return (
        <>
            <Container>
                <Grid container>
                    <Grid item xs={12} sm={12} md={12} lg={12}>
                        <TableContainer style={{marginTop : "50px"}} component={Paper}>
                            <Table sx ={{minWidth : 650}} >
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Id</TableCell>
                                        <TableCell align="right">Name</TableCell>
                                        <TableCell align="right">Username </TableCell>
                                        <TableCell align="right">Email</TableCell>
                                        <TableCell align="right">Phone</TableCell>
                                        <TableCell align="right">Website</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {rows.map((row) => (
                                        <TableRow
                                            key={row.id}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            
                                            <TableCell  component="th" scope="row">
                                                {row.id}
                                            </TableCell>
                                            <TableCell align="right">{row.name}</TableCell>
                                            <TableCell align="right">{row.username}</TableCell>
                                            <TableCell align="right">{row.email}</TableCell>
                                            <TableCell align="right">{row.phone}</TableCell>
                                            <TableCell align="right">{row.website}</TableCell>
                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </Grid>
                </Grid>
                <Grid container justifyContent={"end"}>
                    <Pagination  count={totalPage} defaultPage={curPage} onChange={handleChangePage}/>
                </Grid>
            </Container>
        </>

    );
}

export default DataTable